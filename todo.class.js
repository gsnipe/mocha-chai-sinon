module.exports = class Todo {

    constructor () {
        this.util = {
            trimTodoName : this._trimTodoName
        }
    }

    _trimTodoName (text) {
        return text.trim();
    }

}